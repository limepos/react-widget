import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import WidgetChager  from "../../../components/WidgetCharger"
import { components } from 'react-select';
import { componentFromStreamWithConfig } from 'recompose';

class SamplePage extends React.Component {

  render() {
    return (
      <div className="app-wrapper">
        {/* <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.samplePage"/>}/> */}
           <WidgetChager/>
      </div>
    );
  }
}

export default SamplePage;