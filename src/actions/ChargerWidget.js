import {FETCH_ALL_CHARGERS, SET_CURRENT_CHARGER } from "../constants/ActionTypes";

export const fetchAllChargers = () => {
  return {
    type: FETCH_ALL_CHARGERS
  }
};

export const setCurrentCharger = () => {
    return {
      type: SET_CURRENT_CHARGER
    }
  };
  
