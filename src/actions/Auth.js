import {
  FETCH_ERROR,
  FETCH_START,
  FETCH_SUCCESS,
  INIT_URL,
  SIGNOUT_USER_SUCCESS,
  USER_DATA,
  USER_TOKEN_SET
} from "../constants/ActionTypes";
import axios from 'util/Api'

export const setInitUrl = (url) => {
  return {
    type: INIT_URL,
    payload: url
  };
};

export const userSignUp = ({name, email, password}) => {
  console.log(name, email, password);
  return (dispatch) => {
    dispatch({type: FETCH_START});
    axios.post('auth/register', {
        email: email,
        password: password,
        name: name
      }
    ).then(({data}) => {
      console.log("data:", data);
      if (data.result) {
        localStorage.setItem("token", JSON.stringify(data.access));
        axios.defaults.headers.common['access-token'] = "Bearer " + data.access;
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: USER_TOKEN_SET, payload: data.access});
        dispatch({type: USER_DATA, payload: data.user});
      } else {
        console.log("payload: data.error", data.error);
        dispatch({type: FETCH_ERROR, payload: "Network Error 1"});
      }
    }).catch(function (error) {
      dispatch({type: FETCH_ERROR, payload: error.message});
      console.log("Error:auth/register:", error.message);
    });
  }
};

export const userSignIn = ({name, password}) => {
   return (dispatch) => {
  //   dispatch({type: FETCH_START});
  //   dispatch({type: FETCH_ERROR, payload: axios.INIT_URL});
  //   axios.post('api/token', {
  //       username: name,
  //       password: password
  //     }
  //   )
  //   .then(({data}) => {
  //     console.log("userSignIn: ", data);
  //     if (data.access) {
  //       console.log("data.access:", data.access);
  //       localStorage.setItem("token", JSON.stringify(data.access));
  //       axios.defaults.headers.common['access-token'] = "Bearer " + data.access;
  //       dispatch({type: FETCH_SUCCESS});
  //       dispatch({type: USER_TOKEN_SET, payload: data.access});
  //     } else {
  //       console.log("data.result: no");
  //       dispatch({type: FETCH_ERROR, payload: data.detail});
  //     }
  //   }).catch(function (error) {
  //     dispatch({type: FETCH_ERROR, payload: error.message});
  //     console.log("Error:api/token/:", error.message);
  //   });

        dispatch({type: FETCH_SUCCESS});
        dispatch({type: USER_TOKEN_SET, payload: {}});
  }
};

export const getUser = () => {
  return (dispatch) => {
    // dispatch({type: FETCH_START});
    // console.log("auth/me:...");
    // axios.post('auth/me',
    // ).then(({data}) => {
    //   console.log("userSignIn: ", data);
    //   if (data.user_id) {
    //     dispatch({type: FETCH_SUCCESS});
    //     dispatch({type: USER_DATA, payload: data.username});
    //     localStorage.setItem("user_id", data.user_id);
    //     localStorage.setItem("username", data.username);
    //     localStorage.setItem("first_name", data.first_name);
    //     localStorage.setItem("last_name", data.last_name);
    //     localStorage.setItem("email", data.email);
    //   } else {
    //     console.log("auth/me::" + data);
    //     dispatch({type: FETCH_ERROR, payload: data.message});
    //   }
    // }).catch(function (error) {
    //   dispatch({type: FETCH_ERROR, payload: error.message});
    //   console.log("Error:auth/me:", error.message);
    // });

         dispatch({type: FETCH_SUCCESS});
        dispatch({type: USER_DATA, payload: "lakahan"});
        localStorage.setItem("user_id", "1");
        localStorage.setItem("username","Lakhan");
        localStorage.setItem("first_name","lakhan");
        localStorage.setItem("last_name", "Kewat");
        localStorage.setItem("email", "lakhan@appson.com");
  }
};

export const userSignOut = () => {
  return (dispatch) => {
    dispatch({type: FETCH_START});
    axios.post('auth/logout',
    ).then(({data}) => {
      if (data.user_id) {
        dispatch({type: FETCH_SUCCESS});
        //localStorage.removeItem("token");
        localStorage.clear()
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: SIGNOUT_USER_SUCCESS});
      } else {
        dispatch({type: FETCH_ERROR, payload: data.message});
      }
    }).catch(function (error) {
      console.log("Error:auth/logout:", error.message);
      if (error.response.status == 401) {
        dispatch({type: FETCH_SUCCESS});
        localStorage.clear()
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: SIGNOUT_USER_SUCCESS});
      } else{
        dispatch({type: FETCH_ERROR, payload: error.message});
      }
    });
  }
};
