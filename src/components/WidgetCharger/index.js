import React from "react";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
//import {setCurrentCharger} from '../../actions/ChargerWidget';
import "./WidgetStyle.css";
import Van from "./vahiclesimages/van.png";
import Bus from "./vahiclesimages/bus.png";
import Car from "./vahiclesimages/car.png";
import Setting from "./vahiclesimages/setting.png";
import Available from "./vahiclesimages/Available.png";
import Battery from "./battery";
import data from './data.json';
import GaugeChart from 'react-gauge-chart';
class WidgetChager extends React.PureComponent {

  constructor(props) {
              super(props);
       }
       
      showdetails =(data) => {
      
         let p = (data.SoC / 100)
          
         this.props.changeName({
          isActive : data.idConnector,
          activeparcent: p,
          min : data.minValue,
          max: data.maxValue,
          current: data.currentValue,
          typeVehicleCharging: data.typeVehicleCharging,
          status : data.connectorState
        })
         
      }
  render() {

          const parkings = this.props.data.map(item =>

                       <div className= {`chargeattach ${ item.numberOfConnectors  == 4 ? 'chagerspace_four': item.numberOfConnectors  == 3 ? 'chagerspace_three': item.numberOfConnectors  == 2 ? 'chagerspace_two' : item.numberOfConnectors  == 1 ? 'chagerspace_one': ''}`}>
                          { item.numberOfConnectors > 0 ?  <div  onClick={() => this.showdetails(item.connector1)} className={`parking c1 ${ this.props.isActive == item.connector1.idConnector ? 'active': ''}`}>
                         
                            { item.connector1.connectorState ==  'Charging' || item.connector1.connectorState == 'coming soon'  ? <Battery parcent={item.connector1.SoC} /> : '' }
                          
                            { item.connector1.connectorState ==  'in maintenance' ? <img src={Setting} className="maintenance" /> : item.connector1.connectorState ==  'Charging' ||  item.connector1.connectorState == 'coming soon'  ? <img src={ item.connector1.typeVehicleCharging == 'Car' ? Car : item.connector1.typeVehicleCharging == 'Van' ? Van : item.connector1.typeVehicleCharging == 'Bus' ? Bus : '' } /> : item.connector1.connectorState ==  'available' ? <img src={Available} className="available"/>: '' }
                           
                       </div> : ''} 
                        { item.numberOfConnectors > 1 ?
                        <div onClick={() => this.showdetails(item.connector2)} className={`parking c2 ${ this.props.isActive == item.connector2.idConnector ? 'active': ''}`}>
                            { item.connector2.connectorState ==  'Charging' || item.connector2.connectorState == 'coming soon' ? <Battery parcent={item.connector2.SoC} /> : '' }
                          
                          { item.connector2.connectorState ==  'in maintenance' ? <img src={Setting} className="maintenance" /> : item.connector2.connectorState ==  'Charging' ||  item.connector2.connectorState == 'coming soon'  ? <img src={ item.connector2.typeVehicleCharging == 'Car' ? Car : item.connector2.typeVehicleCharging == 'Van' ? Van : item.connector2.typeVehicleCharging == 'Bus' ? Bus : '' } /> : item.connector2.connectorState ==  'available' ? <img src={Available} className="available"/>: '' }
                           </div> : ''} 
                        { item.numberOfConnectors > 2 ?
                        <div onClick={() => this.showdetails(item.connector3)} className={`parking c3 ${ this.props.isActive === item.connector3.idConnector ? 'active': ''}`}>
                             
                             { item.connector3.connectorState ==  'Charging' || item.connector3.connectorState == 'coming soon'  ? <Battery parcent={item.connector3.SoC} /> : '' }
                          
                          { item.connector3.connectorState ==  'in maintenance' ? <img src={Setting} className="maintenance" /> : item.connector3.connectorState ==  'Charging' ||  item.connector3.connectorState == 'coming soon'  ? <img src={ item.connector3.typeVehicleCharging == 'Car' ? Car : item.connector3.typeVehicleCharging == 'Van' ? Van : item.connector3.typeVehicleCharging == 'Bus' ? Bus : '' } /> : item.connector3.connectorState ==  'available' ? <img src={Available} className="available"/>: '' }
                               </div>  
                         : ''}
                         { item.numberOfConnectors > 3 ?
                        <div onClick={() => this.showdetails(item.connector4)} className={`parking c4 ${ this.props.isActive == item.connector4.idConnector ? 'active': ''}`}>
                              { item.connector4.connectorState ==  'Charging' ||  item.connector4.connectorState == 'coming soon'  ? <Battery parcent={item.connector4.SoC} /> : '' }
                          
                          { item.connector4.connectorState ==  'in maintenance' ? <img src={Setting} className="maintenance"/> : item.connector4.connectorState ==  'Charging' ||  item.connector4.connectorState == 'coming soon'  ? <img src={ item.connector4.typeVehicleCharging == 'Car' ? Car : item.connector4.typeVehicleCharging == 'Van' ? Van : item.connector4.typeVehicleCharging == 'Bus' ? Bus : '' } /> : item.connector4.connectorState ==  'available' ? <img src={Available} className="available" />: '' }
                           </div> : '' }
                    </div>
          );
    return (
      <div className="row"> 
        <div className="col-md-9"> 
               
              <div className="widgetarea box">

                  {parkings}

              </div>
        </div>
        <div className="col-md-3 detailsbox"> 
                <div className="box center bark">
                { this.props.status ==  'Charging' || this.props.status == 'coming soon' ? <span><GaugeChart id="gauge-chart2" 
                    nrOfLevels={20} 
                    colors={['#EA4228', '#5BE12C', '#F5CD19', '#11f109']}
                    percent={this.props.activeparcent} 
                    /> 
                <div class="minvalue meter"> {this.props.min } KW </div> 
                <div class="curent meter"> {this.props.current } KW </div>
                <div class="maxvalue meter"> {this.props.max} KW </div> </span> : "" }
                  
                { this.props.status ==  'in maintenance' ? <span><img src={Setting} className="imgs" /> <h3> In Maintenance </h3></span> : '' }
                { this.props.status ==  'available' ? <span><img src={Available} className="imgs" /> <h3> Available </h3> </span> : '' }
                  
              </div>
              { this.props.status ==  'Charging' || this.props.status == 'coming soon' ? 
                
                  <div class="details infoarea box">
                   <p> Energy cargada ultimas 24 horas :123 MWh </p>

                   <p> Energia cargado mes en cueso </p>
                   
                   <p> Potencia maxima registrada </p>

                   <p> Costao estimado a la fecha </p>
                </div>  : <div class="notselected details infoarea box"> </div> }

                { this.props.status ==  'Charging' || this.props.status == 'coming soon' ? 

                <div className="details infoarea box">
                   <h5 className="titlen"> Detaile Carga: </h5>

                   <div className="vechicleBox"> <img src={ this.props.typeVehicleCharging == 'Car' ? Car : this.props.typeVehicleCharging == 'Van' ? Van : this.props.typeVehicleCharging == 'Bus' ? Bus : ''} /> </div>
                    <div className="vechicleinfo">

                    <p> Patente : AR JS 12</p>
                    
                    <p> Hora Inicio Carga : 12:32 </p>

                    <p> SoC Inicio : {this.props.activeparcent * 100 } % </p>

                    <p> Hora Termino programada: 14:17 </p>
                    
                    <p> SoC termino : 100% </p>

                    <p> Potencia actual : {this.props.max} kW</p>
                  </div>
                </div>
                 : <div class="notselected details infoarea box"> </div> }
              
            
        </div>
      </div>

    );

     }
}

const mapStateToProps = (state) => {
  console.log("State", state)
  return {
      isActive :  state.chargerWidget.isActive,
      activeparcent:  state.chargerWidget.activeparcent,
      min : state.chargerWidget.min,
      max: state.chargerWidget.max,
      current: state.chargerWidget.current,
      typeVehicleCharging: state.chargerWidget.typeVehicleCharging,
      status : state.chargerWidget.status,
      data : state.chargerWidget.data
  }
}
const mapDispatchToProps = (dispatch) => { 
    return {
      changeName:(name) => { dispatch({ type: 'SET_CURRENT_CHARGER', payload: name }) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WidgetChager);

