import React from "react";
import "./WidgetStyle.css";

class Battery extends React.PureComponent {

  constructor(props) {
              super(props);   
         }

  render() {

   let p  =  this.props.parcent * 2;



   if(this.props.parcent > 99){
    var animation = "1s";
    var repeatCount="1";
     
   }else{
    
    var animation = "8s";
    var repeatCount="indefinite";
   }
             

    return (
        <div class="bat">
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320.13 150.85">
            
        <path class="cls-1" d=""/>
        
        <rect class="power" x="26" y="26" width="250" height="90">
        <animate 
          attributeName="width" 
          values={`0 ; ${p}; `}
       
          dur={animation}
          repeatCount={repeatCount}
        />
       
        </rect>
    <text x="80" y="90" class="small">{this.props.parcent}%</text>
        </svg>
       </div> 

    );

     }
}
export default Battery;

