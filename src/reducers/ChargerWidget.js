import {FETCH_ALL_CHARGERS, SET_CURRENT_CHARGER } from "../constants/ActionTypes";
import data from '../components/WidgetCharger/data.json';
const iState = {
        isActive : 'data.idConnector',
        activeparcent: 0,
        min : 0,
        max: 0,
        current: 0,
        typeVehicleCharging: "",
        status : '',
        data : data
}

export default (state = iState, action) => {
  switch (action.type) {
    case 'SET_CURRENT_CHARGER':
      return {
        ...state,
        isActive : action.payload.isActive,
        activeparcent: action.payload.activeparcent,
        min : action.payload.min,
        max: action.payload.max,
        current: action.payload.current,
        typeVehicleCharging: action.payload.typeVehicleCharging,
        status : action.payload.status
      };
    default:
      return state;
  }
};
